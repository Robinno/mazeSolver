Wat er gebeurt bij mijn algoritme:

Eerst en vooral heb ik een soort graph geschreven, maar ik heb geen gebruik gemaakt van edges. 
Ik heb gewoon in een private Node klasse een verwijzing gemaakt naar de node ten noorden, oosten, zuiden en westen van de huidige node.
Hierdoor kan ik alle verschillende paden nemen, als ik de eerste en laatste node weet.

Het algoritme zelf:

1. In de klasse ReadMaze wordt de maze uitgelezen uit het bestand en omgezet in een 2-dimensionale array van Char's.

2. In de klasse ConvertMazeToGraph wordt die 2-dimensionale char array omgezet in een graph.
	Een eerste manier van denken zou zijn om op alle witte vakjes een node te plaatsen, maar dit is niet erg effici�nt voor grotere maze's:

						X XXX X
		stel dat dit de maze is: 	XOOOOOX		het zou onnuttig zijn om op alle punten O een Node te plaatsen,
						X XXX X		want er moeten op die punten geen keuzes worden gemaakt.

						X XXX X
		efficienter is: 	 	XO   OX		Beter om enkel op de plaatsen waar een keuze moet worden gemaakt,		
						X XXX X		om te zetten in een Node.
 
	Ik heb een aantal regeltjes bedacht om te weten of een vakje een node moet worden.
	
	a. eerste checken of het vakje een muur is => dan wordt het GEEN Node
	b. Daarna checken of het op de eerste of laatste kolom staat => beginvakje of eindvakje => WEL een Node
	c. Daarna checken of het vakje 2 buren heeft die ook wit zijn. Als het geen 2 buren heeft => WEL een Node
	d. Daarna checken of de buren recht tegenover mekaar liggen. => GEEN Node

	Eenmaal beslist is dat een Node wordt gemaakt, wordt de Node verbonden met andere Nodes die al gemaakt zijn in de maze:
	Gezien de maze doorlopen wordt van links naar rechts, van boven naar onder:
	- Verbinden met de node Links van (op de zelfde rij) ALS er geen muur tussen zit.
	- Verbinden met de node Boven van (in de zelfde kolom) ALS er geen muur tussen zit.

3. Het pad zoeken: We starten op de startNode en nemen steeds het pad naar links. 
	Als we op een bepaald moment op een doodlopend pad komen (we kunnen niet meer naar links gaan, behalve dat we gaan vanwaar we komen), 
	Dan keren we terug in ons pad tot waar er Wel nog een keuze was om een ander pad te nemen.
	Eenmaal we op de eindNode komen, geven we het pad terug in een ArrayList van Nodes.

Om dit pad dan ietwat presenteerbaar te maken naar de gebruiker toe, heb ik nog een stukje code geschreven die dit dan weer omzet in de oplossing in de Maze.
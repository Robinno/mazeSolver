
public class ConvertMazeToGraph {
	private static GraphZonderEdges graph = new GraphZonderEdges();
	private static final char[][] maze = ReadMaze.convertMazeToArray();
	
	private static int[] laatsteElKolom = new int[maze.length];
	private static int laatsteNodeInRij = -1;

	/**
	 * hij doorloop de hele maze van links naar rechts, van boven naar onder. Op
	 * bepaal de punten moet hij een node maken. ik heb enkele regeltjes verzonnen
	 * om een graphnode te maken op die positie: 
	 * 1. als je op een muur zit => geen graphnode maken.
	 * 2. als je daarna op de eerste of laatste kolom zit => wel
	 * 		een graphnode maken: je zit sowieso op de start of het einde 
	 * 3. als je daarna geen 2 buren hebt => Wel een graphnode maken 
	 * 4. als daarna de buren niet rechttegenover zitten: ook een node maken.
	 * 
	 * @return de gemaakte graph
	 * @throws unvalidPositionException 
	 * @throws NotInLignOfSightException 
	 */
	public static GraphZonderEdges getGraph() throws NotInLignOfSightException, unvalidPositionException {

		int countNeighbours = 0;
		boolean tegenover = false;
		
		//initialiseren vd array laatsteElKolom
		for(int i = 0; i <maze.length; i++) {
			laatsteElKolom[i] = -1;
		}

		for (int i = 1; i < maze.length - 1; i++) {// eerste en laatste rij moet je niet checken, sowieso muur
			for (int j = 0; j < maze.length; j++) {// beide groottes van de kolommen en rijen zijn gelijk

				if ((maze[i][j] == 'X' || maze[i][j] == 'x')) {// als op muur
					laatsteNodeInRij = -1; //geen verbindende node meer vanuit deze positie.
					laatsteElKolom[j] = -1;//-1 betekend dat er geen node boven staat.
				}else {
					if (j == 0 || j == maze.length - 1) {// eerste of laatste kolom
						createNode(i, j);
					} else {
						countNeighbours = 0;

						//2 buren?
						if (!(maze[i - 1][j] == 'X' || maze[i - 1][j] == 'x')) {countNeighbours++;}
						if (!(maze[i][j - 1] == 'X' || maze[i][j - 1] == 'x')) {countNeighbours++;}
						if (!(maze[i + 1][j] == 'X' || maze[i + 1][j] == 'x')) {countNeighbours++;}
						if (!(maze[i][j + 1] == 'X' || maze[i][j + 1] == 'x')) {countNeighbours++;}
	
						if (countNeighbours != 2) {
							createNode(i, j);
	
						} else
							tegenover = false;
	
							//liggen de 2 buren tegenover mekaar?
							if (!(maze[i - 1][j] == 'X' || maze[i - 1][j] == 'x')
									&& !(maze[i + 1][j] == 'X' || maze[i + 1][j] == 'x')) {
								tegenover = true;
							}
							if (!(maze[i][j - 1] == 'X' || maze[i][j - 1] == 'x')
									&& !(maze[i][j + 1] == 'X' || maze[i][j + 1] == 'x')) {
								tegenover = true;
							}
		
							if (!tegenover) {// buren niet tegenover mekaar
		
								createNode(i, j);
	
							}
						}
					}
	
					//System.out.print(maze[i][j]);
				}
				//System.out.println("");
			}

		return graph;
	}

	/**
	 * deze methode maakt een Node aan op de positie x = i, y = j. de node wordt ook verbonden met andere nodes indien mogelijk.
	 * @param i
	 * @param j
	 * @throws NotInLignOfSightException
	 * @throws unvalidPositionException
	 */
	private static void createNode(int i, int j) throws NotInLignOfSightException, unvalidPositionException {
		graph.createNode(j, i);//j = de kolom, i = de rij;	
		
		//de connections
		
		if(laatsteElKolom[j] != -1) {
			graph.connectNodes(new NodePosition(j, i), new NodePosition(j, laatsteElKolom[j]));
		}
		
		if(laatsteNodeInRij != -1) {
			graph.connectNodes(new NodePosition(j,i), new NodePosition(laatsteNodeInRij, i));
		}
		
		//variabelen aanpassen
		
		laatsteNodeInRij = j;
		laatsteElKolom[j] = i;	
	}
}

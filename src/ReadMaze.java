import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class ReadMaze {

	private static final String FILENAME = "maze.txt";

	/**
	 * @return een ArrayList van strings uit de file
	 */
	private static ArrayList<String> convertMazeToText() {

		ArrayList<String> maze = new ArrayList<String>();
		BufferedReader br = null;
		FileReader fr = null;

		try {

			fr = new FileReader(FILENAME);
			br = new BufferedReader(fr);

			String sCurrentLine;

			while ((sCurrentLine = br.readLine()) != null) {
				maze.add(sCurrentLine);
			}
			

		} catch (IOException e) {

			e.printStackTrace();

		} finally {

			try {

				if (br != null)
					br.close();

				if (fr != null)
					fr.close();

			} catch (IOException ex) {

				ex.printStackTrace();

			}

		}
		
		return maze;

	}
	
	/**
	 * @return een matrix van karakters uitgelezen uit het bestand.
	 */
	public static char[][] convertMazeToArray(){
		
		ArrayList<String> mazeStrings = ReadMaze.convertMazeToText();	
		
		int size = mazeStrings.size();
		
		char[][] matrix = new char[size][size]; //ik weet dat de maze vierkant is.		
		
		for(int i = 0; i < size; i++) {
			matrix[i] = mazeStrings.get(i).toCharArray();
		}
		
		return matrix;
		
	}

}

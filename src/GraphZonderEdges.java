import java.util.ArrayList;

public class GraphZonderEdges {

	private ArrayList<Node> alleNodes = new ArrayList<Node>();
	private Node firstNode;
	private Node lastNode;

	/**
	 * creates a new node on x, y
	 * @param x
	 * @param y
	 */
	public void createNode(int x, int y) {
		alleNodes.add(new Node(x, y));
	}

	/**
	 * @return de eerste node van de maze = de ingang.
	 */
	public NodePosition getFirstNode() {
		findFirstNode();
		return firstNode.getPosition();
	}

	/**
	 * @return de laatste node van de maze = de uitgang
	 */
	public NodePosition getLastNode() {
		findLastNode();
		return lastNode.getPosition();
	}

	/**
	 * zet de parameter 'visited' op true van node op positie pos
	 * @param pos
	 * @throws unvalidPositionException
	 */
	public void setVisited(NodePosition pos) throws unvalidPositionException {
		Node n = getNodeAtPosition(pos);
		n.visited();
	}

	/**
	 * zet de parameter 'visited' van ALLE nodes terug op false
	 */
	public void resetAllVisited() {
		for (Node n : alleNodes) {
			n.resetVisited();
		}
	}

	/**
	 * @param pos
	 * @return true als parameter visited van de ndoe op positie pos = false
	 * @throws unvalidPositionException
	 */
	public boolean isVisited(NodePosition pos) throws unvalidPositionException {
		Node n = getNodeAtPosition(pos);
		return n.isVisited();
	}

	/**
	 * connects two nodes on positions pos1, pos2
	 * @param pos1
	 * @param pos2
	 * @throws NotInLignOfSightException
	 * @throws unvalidPositionException
	 */
	public void connectNodes(NodePosition pos1, NodePosition pos2)
			throws NotInLignOfSightException, unvalidPositionException {
		connect(getNodeAtPosition(pos1), getNodeAtPosition(pos2));
	}

	/**
	 * @param pos
	 * @return de positie van de node ten noorden van positie pos
	 * @throws unvalidPositionException
	 */
	public NodePosition getNoord(NodePosition pos) throws unvalidPositionException {
		Node n = this.getNodeAtPosition(pos);
		if (n.getNoord() != null) {
			return n.getNoord().getPosition();
		} else {
			return null;
		}
	}

	/**
	 * @param pos
	 * @return de positie van de node ten zuiden van positie pos
	 * @throws unvalidPositionException
	 */
	public NodePosition getZuid(NodePosition pos) throws unvalidPositionException {
		Node n = this.getNodeAtPosition(pos);
		if (n.getZuid() != null) {
			return n.getZuid().getPosition();
		} else {
			return null;
		}
	}

	/**
	 * @param pos
	 * @return de positie van de node ten westen van positie pos
	 * @throws unvalidPositionException
	 */
	public NodePosition getWest(NodePosition pos) throws unvalidPositionException {
		Node n = this.getNodeAtPosition(pos);
		if (n.getWest() != null) {
			return n.getWest().getPosition();
		} else {
			return null;
		}
	}

	/**
	 * @param pos
	 * @return de positie van de node ten oosten van positie pos
	 * @throws unvalidPositionException
	 */
	public NodePosition getOost(NodePosition pos) throws unvalidPositionException {
		Node n = this.getNodeAtPosition(pos);
		if (n.getOost() != null) {
			return n.getOost().getPosition();
		} else {
			return null;
		}
	}

	/**
	 * private methode
	 * @param pos
	 * @return de node op de positie pos
	 * @throws unvalidPositionException
	 */
	private Node getNodeAtPosition(NodePosition pos) throws unvalidPositionException {
		for (Node n : alleNodes) {
			if (pos.getX() == n.getX() && pos.getY() == n.getY()) {
				return n;
			}
		}
		throw new unvalidPositionException();
	}

	/**
	 * private methode: connects node1 with node2
	 * @param node1
	 * @param node2
	 * @throws NotInLignOfSightException
	 */
	private void connect(Node node1, Node node2) throws NotInLignOfSightException {
		if (node1.getY() == node2.getY()) {
			if (node1.getX() < node2.getX()) {
				node1.setOost(node2);
				node2.setWest(node1);
			}
			if (node1.getX() > node2.getX()) {
				node1.setWest(node2);
				node2.setOost(node1);
			}
		} else if (node1.getX() == node2.getX()) {
			if (node1.getY() < node2.getY()) {
				node1.setZuid(node2);
				node2.setNoord(node1);
			}
			if (node1.getY() > node2.getY()) {
				node1.setNoord(node2);
				node2.setZuid(node1);
			}
		} else {
			throw new NotInLignOfSightException();
		}
	}

	/**
	 * private methode: vindt de eerste node
	 */
	private void findFirstNode() {
		for (Node n : alleNodes) {
			if (n.getX() == 0) {
				firstNode = n;
				return;
			}
		}
	}

	/**
	 * private methode: vindt de laatste node
	 */
	private void findLastNode() {
		lastNode = alleNodes.get(0);
		for (Node n : alleNodes) {
			if (n.getX() > lastNode.getX()) {
				lastNode = n;
			}
		}
	}

	private class Node {

		int x, y;
		Node noord, oost, zuid, west = null;
		boolean visited = false;

		//constructor
		public Node(int x, int y) {
			this.x = x;
			this.y = y;
		}

		/**
		 * @return x-coord van de node
		 */
		public int getX() {
			return x;
		}

		/**
		 * @return y-coord van de node
		 */
		public int getY() {
			return y;
		}

		/**
		 * @return nodepositie van de node
		 */
		public NodePosition getPosition() {
			return new NodePosition(x, y);
		}

		/**
		 * zet visited op true
		 */
		public void visited() {
			visited = true;
		}

		/**
		 * zet visited op false
		 */
		public void resetVisited() {
			visited = false;
		}

		/**
		 * @return visited
		 */
		public boolean isVisited() {
			return visited;
		}

		/**
		 * @return de node ten noorden van deze node
		 */
		public Node getNoord() {
			return noord;
		}

		/**
		 * zet de node ten noorden van deze node op de gegeven node
		 * @param noord
		 */
		public void setNoord(Node noord) {
			this.noord = noord;
		}

		/**
		 * @return de node ten oosten van deze node
		 */
		public Node getOost() {
			return oost;
		}

		/**
		 * zet de node ten oosten van deze node op de gegeven node
		 * @param noord
		 */
		public void setOost(Node oost) {
			this.oost = oost;
		}

		/**
		 * @return de node ten zuiden van deze node
		 */
		public Node getZuid() {
			return zuid;
		}

		/**
		 * zet de node ten zuiden van deze node op de gegeven node
		 * @param noord
		 */
		public void setZuid(Node zuid) {
			this.zuid = zuid;
		}

		/**
		 * @return de node ten westen van deze node
		 */
		public Node getWest() {
			return west;
		}

		/**
		 * zet de node ten westen van deze node op de gegeven node
		 * @param noord
		 */
		public void setWest(Node west) {
			this.west = west;
		}

	}
}

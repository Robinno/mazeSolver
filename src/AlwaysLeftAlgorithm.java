import java.util.ArrayList;

public class AlwaysLeftAlgorithm {

	private static GraphZonderEdges graph;

	/**
	 * Dit is de static init block. Dit was nodig omdat het mogelijk is dat 'getgraph' een exception werpt.
	 */
	static {// Static init block
		try {
			graph = ConvertMazeToGraph.getGraph();
		} catch (Exception e) {
			System.out.println("er is iets misgegaan...");
		} // end try-catch
	} // end static init block

	private static ArrayList<NodePosition> path = new ArrayList<NodePosition>();

	/**
	 * deze methode geeft het pad terug over de nodepositions doorheen de maze
	 * @return
	 * @throws unvalidPositionException
	 */
	public static ArrayList<NodePosition> getPath() throws unvalidPositionException {
		NodePosition eersteNode = graph.getFirstNode();
		NodePosition laatsteNode = graph.getLastNode();

		path.add(eersteNode);
		graph.setVisited(eersteNode);

		path.add(graph.getOost(eersteNode)); // eerste node ligt altijd oost van de ingang.
		graph.setVisited(graph.getOost(eersteNode));

		while (!(path.get(path.size() - 1).getX() == laatsteNode.getX()
				&& path.get(path.size() - 1).getY() == laatsteNode.getY())) { // het laatste element
			NodePosition left = getLeft(path.get(path.size() - 1), path.get(path.size() - 2)); // laatste en voorlaatste
																								// element teruggeven
			if (left != null) {
				path.add(left);
				graph.setVisited(left);
			} else {
				path.remove(path.get(path.size() - 1)); // remove last object from path
			}
		}

		return path;
	}
	
	/**
	 * deze methode zoekt de node die naar links gaat, maar deze mag nog niet gevisited zijn. 
	 * dit kan waarschijnlijk wel effici�nter ge�mplementeerd worden.
	 * Als er geen linkernode is (doodlopend), dan geeft de methode 'null' terug.
	 * @param pos
	 * @param komendvan
	 * @return NodePosition nodenaar links.
	 * @throws unvalidPositionException
	 */
	
	private static NodePosition getLeft(NodePosition pos, NodePosition komendvan) throws unvalidPositionException {

		NodePosition noord = graph.getNoord(pos);
		NodePosition west = graph.getWest(pos);
		NodePosition zuid = graph.getZuid(pos);
		NodePosition oost = graph.getOost(pos);

		// hieronder fout. Je mag komendvan een noord niet rechtstreeks vergelijken.
		// Omdat ze op dezelfde plaats staan betekend niet dat ze hetzelfde zijn.
		if (noord != null) {
			if (komendvan.getX() == noord.getX() && komendvan.getY() == noord.getY()) {
				if (oost != null && !graph.isVisited(oost))
					return oost;
				else if (zuid != null && !graph.isVisited(zuid))
					return zuid;
				else if (west != null && !graph.isVisited(west))
					return west;
				else
					return null;
			}
		}

		if (oost != null) {
			if (komendvan.getX() == oost.getX() && komendvan.getY() == oost.getY()) {
				if (zuid != null && !graph.isVisited(zuid))
					return zuid;
				else if (west != null && !graph.isVisited(west))
					return west;
				else if (noord != null && !graph.isVisited(noord))
					return noord;
				else
					return null;
			}
		}

		if (zuid != null) {
			if (komendvan.getX() == zuid.getX() && komendvan.getY() == zuid.getY()) {
				if (west != null && !graph.isVisited(west))
					return west;
				else if (noord != null && !graph.isVisited(noord))
					return noord;
				else if (oost != null && !graph.isVisited(oost))
					return oost;
				else
					return null;
			}
		}

		if (west != null) {
			if (komendvan.getX() == west.getX() && komendvan.getY() == west.getY()) {
				if (noord != null && !graph.isVisited(noord))
					return noord;
				else if (oost != null && !graph.isVisited(oost))
					return oost;
				else if (zuid != null && !graph.isVisited(zuid))
					return zuid;
				else
					return null;
			}
		}

		return null;// ergens een foutje gebeurt
	}
	
	
	/**
	 * aangezien de nodes enkel staan op de cruciale punten, moet het pad ook gemaakt worden tussen tussen de nodes in.
	 * @param solvedMaze
	 * @throws unvalidPositionException
	 */
	private static void convertNodesToPathInMaze(char[][] solvedMaze) throws unvalidPositionException {

		ArrayList<NodePosition> list = getPath();

		NodePosition pos = list.get(0);
		NodePosition vorigepos;

		solvedMaze[pos.getY()][pos.getX()] = '�';

		// this complicated for loop is mostly for the lines between the nodes

		for (int i = 1; i < path.size(); i++) {
			vorigepos = list.get(i - 1);
			pos = list.get(i);

			if (vorigepos.getX() == pos.getX()) {
				int huidigehoogte = vorigepos.getY();
				while (huidigehoogte != pos.getY()) {
					if (vorigepos.getY() < pos.getY()) {
						huidigehoogte++;
					} else {
						huidigehoogte--;
					}

					solvedMaze[huidigehoogte][pos.getX()] = '�';
				}
			} else {
				int huidigehoogte = vorigepos.getX();
				while (huidigehoogte != pos.getX()) {
					if (vorigepos.getX() < pos.getX()) {
						huidigehoogte++;
					} else {
						huidigehoogte--;
					}
					
					solvedMaze[pos.getY()][huidigehoogte] = '�';
				}
			}

		}
	}
	
	/**
	 * de main methode die wordt aangeroepen om het programma te starten
	 * @param args
	 * @throws unvalidPositionException
	 */
	public static void main(String[] args) throws unvalidPositionException {

		char[][] solvedMaze = ReadMaze.convertMazeToArray();

		System.out.println("the maze : ");
		System.out.println("");

		// printing the maze

		for (int i = 0; i < solvedMaze.length; i++) {
			for (int j = 0; j < solvedMaze.length; j++) {// beide groottes van de kolommen en rijen zijn gelijk

				System.out.print(solvedMaze[i][j]);
			}
			System.out.println("");
		}

		// adding the solution to the matrix

		convertNodesToPathInMaze(solvedMaze);

		// printing the solution

		System.out.println("");
		System.out.println("the solution : ");
		System.out.println("");

		for (int i = 0; i < solvedMaze.length; i++) {
			for (int j = 0; j < solvedMaze.length; j++) {// beide groottes van de kolommen en rijen zijn gelijk

				System.out.print(solvedMaze[i][j]);
			}
			System.out.println("");
		}

	}

}
